package com.company.service3;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;

@JsonPropertyOrder({"craneBulk", "craneLiquid", "craneContainer", "penalty", "quantityUnloadingVessel", "maximumUnloadingDelay", "averageUnloadingDelay", "averageWaitingTimeQueue", "averageLengthUnloadingQueue", "offloads"})
public class Statistic {
    public static final int COST_CRANE = 30000;
    private final ArrayList<Unloading> mOffloads = new ArrayList<>();
    private int mCraneBulk;
    private int mCraneLiquid;
    private int mCraneContainer;
    private int mPenalty = 0;
    private int mAverageLengthUnloadingQueue;
    private int mQuantityLengthUnloadingQueue = 0;
    private int mAverageWaitingTimeQueue;
    private int mQuantityWaitingTimeQueue = 0;
    private int mMaximumUnloadingDelay = 0;
    private int mAverageUnloadingDelay;
    private int mQuantityUnloadingDelay = 0;

    public Statistic(int craneBulk, int craneLiquid, int craneContainer) {
        this.mCraneBulk = craneBulk;
        this.mCraneLiquid = craneLiquid;
        this.mCraneContainer = craneContainer;
        mPenalty = (craneBulk + craneLiquid  + craneContainer) * COST_CRANE;
    }

    Statistic() {

    }

    public static int compare(Statistic statistic1, Statistic statistic2) {
        return Integer.compare(statistic1.mPenalty, statistic2.mPenalty);
    }

    public int getQuantityUnloadingVessel() {
        return mOffloads.size();
    }

    public int getPenalty() {
        return mPenalty;
    }

    public void addPenalty(int penalty) {
        this.mPenalty += penalty;
    }

    public ArrayList<Unloading> getOffloads() {
        return mOffloads;
    }

    public int getAverageLengthUnloadingQueue() {
        if (mQuantityLengthUnloadingQueue != 0) {
            return mAverageLengthUnloadingQueue / mQuantityLengthUnloadingQueue;
        }
        return 0;
    }


    public void addAverageLengthUnloadingQueue(int averageLengthUnloadingQueue) {
        mQuantityLengthUnloadingQueue++;
        this.mAverageLengthUnloadingQueue += averageLengthUnloadingQueue;
    }

    public int getAverageWaitingTimeQueue() {
        if (mQuantityWaitingTimeQueue != 0) {
            return mAverageWaitingTimeQueue / mQuantityWaitingTimeQueue;
        }
        return 0;
    }

    public void addAverageWaitingTimeQueue(int averageWaitingTimeQueue) {
        mQuantityWaitingTimeQueue++;
        this.mAverageWaitingTimeQueue += averageWaitingTimeQueue;
    }

    public int getMaximumUnloadingDelay() {
        return mMaximumUnloadingDelay;
    }


    public int getAverageUnloadingDelay() {
        if (mQuantityUnloadingDelay != 0 && getQuantityUnloadingVessel() != 0) {
            return mAverageUnloadingDelay / getQuantityUnloadingVessel();
        }
        return 0;
    }

    public void addAverageUnloadingDelay(int averageUnloadingDelay) {
        mQuantityUnloadingDelay++;
        mMaximumUnloadingDelay = Math.max(mMaximumUnloadingDelay, averageUnloadingDelay);
        this.mAverageUnloadingDelay += averageUnloadingDelay;
    }

    public int getCraneBulk() {
        return mCraneBulk;
    }

    public int getCraneLiquid() {
        return mCraneLiquid;
    }

    public int getCraneContainer() {
        return mCraneContainer;
    }
}
