package com.company.service1;

public class MyDuration extends MyDate {
    public MyDuration(int minute) {
        super(minute);
    }

    public MyDuration(MyDuration date) {
        mDay = date.mDay;
        mHour = date.mHour;
        mMinute = date.mMinute;
    }
    @Override
    public int getDay() {
        return mDay;
    }
}
